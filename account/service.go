package account

import (
	"context"
	"github.com/go-kit/kit/log"
)

type Service interface {
	CreateUser(ctx context.Context, email string, password string) (string, error)
	GetUser(ctx context.Context, id string) (*User, error)
	ListUser(ctx context.Context) ([]User, error)
}

func NewService(rep Repository, logger log.Logger) Service {
	return &service{
		repository: rep,
		logger:     logger,
	}
}
