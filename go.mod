module gitlab.com/mrtomyum/kit

go 1.13

require (
	github.com/go-kit/kit v0.9.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/gorilla/mux v1.7.3
	github.com/lib/pq v1.3.0
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0
)
